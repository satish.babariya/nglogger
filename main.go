package main

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func main() {

	port := os.Getenv("PORT")

	if port == "" {
		//log.Fatal("$PORT must be set")
		port = "8090"
	}

	http.Handle("/", http.FileServer(http.Dir("./assets/client")))

	http.HandleFunc("/_ws", func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			http.Error(w, "Failed websocket upgrade", 400)
			log.Fatal(err)
			return
		}
		log.Println("Here's some text that the server is urgently awaiting!")

		data := []byte(`
{

  "Id":"5be83ead0000000c",
  "Duration":7907190,
  "Start":1541947053,
  "RemoteAddr":"117.212.128.180",
  "Req":{
    "Raw":"UE9TVCAvdG9kb3MgSFRUUC8xLjENCkNvbnRlbnQtVHlwZTogYXBwbGljYXRpb24vanNvbg0KY2FjaGUtY29udHJvbDogbm8tY2FjaGUNClBvc3RtYW4tVG9rZW46IGU1MzIzMDI0LWMyODUtNGE0Ny1hODFiLWI4MjZmODZiY2RmOA0KVXNlci1BZ2VudDogUG9zdG1hblJ1bnRpbWUvNy40LjANCkFjY2VwdDogKi8qDQpIb3N0OiAzZTIyMTI2NS5uZ3Jvay5pbw0KYWNjZXB0LWVuY29kaW5nOiBnemlwLCBkZWZsYXRlDQpjb250ZW50LWxlbmd0aDogMjYNClgtRm9yd2FyZGVkLUZvcjogMTE3LjIxMi4xMjguMTgwDQoNCnsKCSJ0aXRsZSI6IkhlbGxvIFdvcmxkIgp9",
    "MethodPath":"POST /todos",
    "Params":{
    },
    "Header":{
      "Accept":[
        "*/*"
      ],
      "Accept-Encoding":[
        "gzip, deflate"
      ],
      "Cache-Control":[
        "no-cache"
      ],
      "Content-Length":[
        "26"
      ],
      "Content-Type":[
        "application/json"
      ],
      "Postman-Token":[
        "e5323024-c285-4a47-a81b-b826f86bcdf8"
      ],
      "User-Agent":[
        "PostmanRuntime/7.4.0"
      ],
      "X-Forwarded-For":[
        "117.212.128.180"
      ]
    },
    "Body":{
      "Binary":false,
      "RawContentType":"application/json",
      "Encoding":"",
      "ContentType":"application/json",
      "Text":"ewoJInRpdGxlIjoiSGVsbG8gV29ybGQiCn0=",
      "Error":"",
      "ErrorOffset":-1,
      "Form":null,
      "Size":26,
      "CapturedSize":26,
      "DecodedSize":26,
      "DisplaySize":26
    },
    "Size":318,
    "CapturedSize":318
  },
  "Resp":{
    "Raw":"SFRUUC8xLjEgMjAwIE9LDQpjb250ZW50LXR5cGU6IGFwcGxpY2F0aW9uL2pzb247IGNoYXJzZXQ9dXRmLTgNCmNvbnRlbnQtbGVuZ3RoOiAzMA0KZGF0ZTogU3VuLCAxMSBOb3YgMjAxOCAxNDozNzozMyBHTVQNCkNvbm5lY3Rpb246IGtlZXAtYWxpdmUNCg0KeyJpZCI6NCwidGl0bGUiOiJIZWxsbyBXb3JsZCJ9",
    "Status":"200 OK",
    "Header":{
      "Connection":[
        "keep-alive"
      ],
      "Content-Length":[
        "30"
      ],
      "Content-Type":[
        "application/json; charset=utf-8"
      ],
      "Date":[
        "Sun, 11 Nov 2018 14:37:33 GMT"
      ]
    },
    "Body":{
      "Binary":false,
      "RawContentType":"application/json; charset=utf-8",
      "Encoding":"",
      "ContentType":"application/json",
      "Text":"eyJpZCI6NCwidGl0bGUiOiJIZWxsbyBXb3JsZCJ9",
      "Error":"",
      "ErrorOffset":-1,
      "Form":null,
      "Size":30,
      "CapturedSize":30,
      "DecodedSize":30,
      "DisplaySize":30
    },
    "Size":177,
    "CapturedSize":177
  }

}
`)

		data2 := []byte(`
{

  "Id":"5be83ead00000008489",
  "Duration":7907190,
  "Start":1541947053,
  "RemoteAddr":"117.212.128.180",
  "Req":{
    "Raw":"UE9TVCAvdG9kb3MgSFRUUC8xLjENCkNvbnRlbnQtVHlwZTogYXBwbGljYXRpb24vanNvbg0KY2FjaGUtY29udHJvbDogbm8tY2FjaGUNClBvc3RtYW4tVG9rZW46IGU1MzIzMDI0LWMyODUtNGE0Ny1hODFiLWI4MjZmODZiY2RmOA0KVXNlci1BZ2VudDogUG9zdG1hblJ1bnRpbWUvNy40LjANCkFjY2VwdDogKi8qDQpIb3N0OiAzZTIyMTI2NS5uZ3Jvay5pbw0KYWNjZXB0LWVuY29kaW5nOiBnemlwLCBkZWZsYXRlDQpjb250ZW50LWxlbmd0aDogMjYNClgtRm9yd2FyZGVkLUZvcjogMTE3LjIxMi4xMjguMTgwDQoNCnsKCSJ0aXRsZSI6IkhlbGxvIFdvcmxkIgp9",
    "MethodPath":"POST /hahaha",
    "Params":{
    },
    "Header":{
      "Accept":[
        "*/*"
      ],
      "Accept-Encoding":[
        "gzip, deflate"
      ],
      "Cache-Control":[
        "no-cache"
      ],
      "Content-Length":[
        "26"
      ],
      "Content-Type":[
        "application/json"
      ],
      "Postman-Token":[
        "e5323024-c285-4a47-a81b-b826f86bcdf8"
      ],
      "User-Agent":[
        "PostmanRuntime/7.4.0"
      ],
      "X-Forwarded-For":[
        "117.212.128.180"
      ]
    },
    "Body":{
      "Binary":false,
      "RawContentType":"application/json",
      "Encoding":"",
      "ContentType":"application/json",
      "Text":"ewoJInRpdGxlIjoiSGVsbG8gV29ybGQiCn0=",
      "Error":"",
      "ErrorOffset":-1,
      "Form":null,
      "Size":26,
      "CapturedSize":26,
      "DecodedSize":26,
      "DisplaySize":26
    },
    "Size":318,
    "CapturedSize":318
  },
  "Resp":{
    "Raw":"SFRUUC8xLjEgMjAwIE9LDQpjb250ZW50LXR5cGU6IGFwcGxpY2F0aW9uL2pzb247IGNoYXJzZXQ9dXRmLTgNCmNvbnRlbnQtbGVuZ3RoOiAzMA0KZGF0ZTogU3VuLCAxMSBOb3YgMjAxOCAxNDozNzozMyBHTVQNCkNvbm5lY3Rpb246IGtlZXAtYWxpdmUNCg0KeyJpZCI6NCwidGl0bGUiOiJIZWxsbyBXb3JsZCJ9",
    "Status":"200 OK",
    "Header":{
      "Connection":[
        "keep-alive"
      ],
      "Content-Length":[
        "30"
      ],
      "Content-Type":[
        "application/json; charset=utf-8"
      ],
      "Date":[
        "Sun, 11 Nov 2018 14:37:33 GMT"
      ]
    },
    "Body":{
      "Binary":false,
      "RawContentType":"application/json; charset=utf-8",
      "Encoding":"",
      "ContentType":"application/json",
      "Text":"eyJpZCI6NCwidGl0bGUiOiJIZWxsbyBXb3JsZCJ9",
      "Error":"",
      "ErrorOffset":-1,
      "Form":null,
      "Size":30,
      "CapturedSize":30,
      "DecodedSize":30,
      "DisplaySize":30
    },
    "Size":177,
    "CapturedSize":177
  }

}
`)

		conn.WriteMessage(websocket.TextMessage, data)
		conn.WriteMessage(websocket.TextMessage, data2)

		msgs := NewBroadcast().Reg()
		for m := range msgs {
			err := conn.WriteMessage(websocket.TextMessage, m.([]byte))
			if err != nil {
				// connection is closedheroku.yml
				log.Println(err)
				break
			}
		}
	})

	http.ListenAndServe(":"+port, nil)
}

type ConnectionContext struct {
	ClientAddr string
}

type HttpRequest struct {
	*http.Request
	BodyBytes []byte
}

type HttpResponse struct {
	*http.Response
	BodyBytes []byte
}

type HttpTxn struct {
	Req         *HttpRequest
	Resp        *HttpResponse
	Start       time.Time
	Duration    time.Duration
	UserCtx     interface{}
	ConnUserCtx interface{}
}

type SerializedTxn struct {
	Id       string
	Duration int64
	Start    int64
	ConnCtx  ConnectionContext
	*HttpTxn `json:"-"`
	Req      SerializedRequest
	Resp     SerializedResponse
}

type SerializedBody struct {
	RawContentType string
	ContentType    string
	Text           string
	Length         int
	Error          string
	ErrorOffset    int
	Form           url.Values
}

type SerializedRequest struct {
	Raw        string
	MethodPath string
	Params     url.Values
	Header     http.Header
	Body       SerializedBody
	Binary     bool
}

type SerializedResponse struct {
	Raw    string
	Status string
	Header http.Header
	Body   SerializedBody
	Binary bool
}

type Broadcast struct {
	listeners []chan interface{}
	reg       chan (chan interface{})
	unreg     chan (chan interface{})
	in        chan interface{}
}

func NewBroadcast() *Broadcast {
	b := &Broadcast{
		listeners: make([]chan interface{}, 0),
		reg:       make(chan (chan interface{})),
		unreg:     make(chan (chan interface{})),
		in:        make(chan interface{}),
	}

	go func() {
		for {
			select {
			case l := <-b.unreg:
				// remove L from b.listeners
				// this operation is slow: O(n) but not used frequently
				// unlike iterating over listeners
				oldListeners := b.listeners
				b.listeners = make([]chan interface{}, 0, len(oldListeners))
				for _, oldL := range oldListeners {
					if l != oldL {
						b.listeners = append(b.listeners, oldL)
					}
				}

			case l := <-b.reg:
				b.listeners = append(b.listeners, l)

			case item := <-b.in:
				for _, l := range b.listeners {
					l <- item
				}
			}
		}
	}()

	return b
}

func (b *Broadcast) In() chan interface{} {
	return b.in
}

func (b *Broadcast) Reg() chan interface{} {
	listener := make(chan interface{})
	b.reg <- listener
	return listener
}

func (b *Broadcast) UnReg(listener chan interface{}) {
	b.unreg <- listener
}
